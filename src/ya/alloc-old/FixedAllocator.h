#pragma once
#include <cstdint>
#include <vector>
#include "Chunk.h"

namespace ya {

template <class U>
class TFixedAllocator
{
  public:
    TFixedAllocator(std::size_t _block_size, U _num_blocks)
        : block_size_{_block_size}
        , num_blocks_{_num_blocks}
    {
        chunks_.reserve(64);
    }

    ~TFixedAllocator() = default;

    void* allocate(std::size_t _block_size)
    {
        assert(block_size_ >= _block_size);
        if (alloc_chunk_ == nullptr || alloc_chunk_->block_available == 0) {
            alloc_chunk_ = nullptr;
            for (Chunk& chunk: chunks_) {
                if (chunk.block_available) {
                    alloc_chunk_ = &chunk;
                    break;
                }
            }
            if (alloc_chunk_ == nullptr) {
                chunks_.emplace_back(block_size_, num_blocks_, DefaultAllocator{});
                dealloc_chunk_ = alloc_chunk_ = &chunks_.back();
            }
        }
        return alloc_chunk_->allocate(block_size_);
    }

    bool deallocate(void* _ptr, std::size_t _block_size)
    {
        if (!dealloc_chunk_ || !dealloc_chunk_->deallocate(_ptr, _block_size)) {
            for (auto rit = chunks_.rbegin(); rit != chunks_.rend(); ++rit) {
                if (rit->deallocate(_ptr, _block_size)) {
                    dealloc_chunk_ = &(*rit);
                    return true;
                }
            }
            return false;
        }
        return true;
    }

  private:
    using Chunks = std::vector<Chunk>;
    /* data */
    const std::size_t block_size_;
    U num_blocks_;
    Chunks chunks_;
    Chunk* alloc_chunk_{};
    Chunk* dealloc_chunk_{};
}; /* class FixedAllocator */

using FixedAllocator = TFixedAllocator<unsigned char>;

} /* namespace ya */
