#pragma once
#include <cstdint>
#include <vector>
#include "FixedAllocator.h"

namespace ya {

class Allocator
{
  public:
    Allocator() = default;
    ~Allocator() = default;

    Allocator(Allocator&&) = delete;
    Allocator& operator=(Allocator&&) = delete;

    void* allocate(std::size_t _size)
    {
        if (_size > 0 && _size <= 0x10000) {
            auto allocator = getAllocator(_size);
            if (allocator.first) {
                return allocator.first->allocate(_size);
            }
        }
        return nullptr;
    }

    bool deallocate(void* _ptr, std::size_t _size)
    {
        if (_size <= 0x10000) {
            auto allocator = getAllocator(_size);
            if (allocator.first) {
                return allocator.first->deallocate(_ptr, _size);
            }
        }
        return false;
    }

  private:
    /* data */
    std::vector<FixedAllocator> pool_;

    static constexpr std::size_t start_size = (1 << 3);

    std::pair<FixedAllocator*, std::size_t> getAllocator(std::size_t _size)
    {
        auto index = getIndex(_size);
        while (index.first >= pool_.size()) {
            auto block_size = start_size << pool_.size();
            unsigned char blocks{255};
            pool_.emplace_back(block_size, blocks);
        }
        return {&pool_[index.first], index.second};
    }

    std::pair<std::size_t, std::size_t> getIndex(std::size_t _size)
    {
        std::size_t i = 0;
        std::size_t limit = start_size;
        for (; limit < _size; limit <<= 1, ++i)
            ;
        return {i, limit};
    }

}; /* class Allocator */

} /* namespace ya */
