#pragma once
#include <cstdint>
#include <cassert>
#include <stdexcept>

namespace ya {

struct DefaultAllocator
{
    static std::size_t total_alloc;
    template <class T>
    T* allocate(std::size_t size)
    {
        if ((total_alloc + size) > (1024 * 1024 * 1024))
            throw std::bad_alloc{};
        total_alloc += size;
        return new T[size];
    }

    template <class T>
    void deallocate(T* ptr)
    {
        delete[] ptr;
    }

    void* allocate(std::size_t size)
    {
        return allocate<unsigned char>(size);
    }
};
std::size_t DefaultAllocator::total_alloc = 0;

template<class U>
struct TChunk
{
    U* pdata{nullptr};
    U first_available_block{0};
    U block_available;
    // :std::size_t size_;

    template <class Alloc>
    TChunk(std::size_t _block_size, U nblocks, Alloc _allocator = DefaultAllocator{})
    {
        auto const size = get_size(_block_size) * nblocks;
        pdata = _allocator.template allocate<U>(size);
        block_available = nblocks;

        auto p = pdata;
        for (U i = 0; i < nblocks; p += _block_size)
            *p = ++i;
    }

    void* allocate(std::size_t _block_size)
    {
        if (!block_available)
            return nullptr;

        auto offset = (first_available_block * get_size(_block_size));
        U* presult = pdata + offset;
        first_available_block = *presult;
        --block_available;
        return presult;
    }

    bool deallocate(void* _ptr, std::size_t _block_size)
    {
        auto to_release = static_cast<U*>(_ptr);
        if (to_release >= pdata/* && to_release < (pdata + size)*/) {
            assert(((to_release - pdata) % _block_size) == 0);
            *to_release = first_available_block;

            first_available_block = static_cast<U>((to_release - pdata) / _block_size);

            assert(first_available_block == (to_release - pdata) / _block_size);

            ++block_available;
            return true;
        }
        return false;
    }

  protected:
    constexpr std::size_t get_size(std::size_t _block_size)
    {
        if ((_block_size % sizeof(U)) == 0) {
            return _block_size / sizeof(U);
        }
        return (_block_size / sizeof(U)) + 1;
    }
}; /* end of struct Chunk */

using Chunk = TChunk<unsigned char>;

} /* namespace ya */

