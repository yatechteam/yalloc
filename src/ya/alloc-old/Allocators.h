#pragma once

#include <cstdint>
#include <cassert>

namespace ya {

template <class T>
void unused(T t)
{
    (void)t;
}

struct MemoryBlock
{
    template <class T>
    T* as()
    {
        assert(sizeof(T) <= length);
        return static_cast<T*>(ptr);
    }

    operator bool() const {
        return ptr != nullptr;
    }

    void* ptr{nullptr};
    std::size_t length{0};
};

struct NullAllocator
{
    MemoryBlock allocate(std::size_t _size)
    {
        return {nullptr, _size};
    }

    void deallocate(MemoryBlock _block)
    {
        assert(!_block.ptr);
    }

    bool owns(MemoryBlock _block)
    {
        return !_block.ptr;
    }
}; /* end of struct NullAllocator */

template <class Primary, class Secondary>
class FallbackAllocator : private Primary, private Secondary
{
  public:
    MemoryBlock allocate(std::size_t _size)
    {
        MemoryBlock block = Primary::allocate(_size);
        if (!block)
            block = Secondary::allocate(_size);
        return block;
    }

    void deallocate(MemoryBlock _block)
    {
        if (Primary::owns(_block)) {
            Primary::deallocate(_block);
        } else {
            Secondary::deallocate(_block);
        }
    }

    bool owns(MemoryBlock _block)
    {
        return Primary::owns(_block) || Secondary::owns(_block);
    }
};

template <uint16_t SIZE, std::size_t ALLIGN = sizeof(long)>
class StackAllocator
{
  public:
    StackAllocator()
    {
    }

    MemoryBlock allocate(std::size_t _size)
    {
        auto size = allign(_size);
        void* ptr{nullptr};
        if ((stack + size) <= SIZE) {
            ptr = &data[stack];
            stack += size;
        }
        return MemoryBlock{ptr, ptr ? _size : 0};
    }

    void deallocate(MemoryBlock _block)
    {
        auto const ptr = _block.as<char>();
        auto const size = allign(_block.length);
        if (ptr + size == data + stack) {
            stack -= size;
        }
    }

    bool owns(MemoryBlock _block)
    {
        auto ptr = _block.as<char>();
        return ((data <= ptr) && ((data + SIZE) >= (ptr + _block.length)));
    }

    void deallocateAll()
    {
        stack = 0;
    }

  private:
    /* data */
    char data[SIZE];
    std::size_t stack{0};

    constexpr std::size_t allign(std::size_t _size)
    {
        auto r = _size % ALLIGN;
        if (r == 0) {
            return _size;
        }
        return _size - r + ALLIGN;
    }
}; /* class StackAllocator */

template<class T>
constexpr bool is_in(T v, T min, T max) {
    return (v >= min && v <= max);
}

template <class A, std::size_t min, std::size_t max, std::size_t batch, std::size_t limit>
class FreelistAllocator
{
  public:
    FreelistAllocator();
    ~FreelistAllocator() {
        deallocateAll();
    }

    MemoryBlock allocate(std::size_t _size)
    {
        MemoryBlock blk;
        if (is_in(_size, min, max))
        {
            blk = pop(_size);
            if (!blk) {
                blk = doAllocate(_size);
            }
        }
        return blk;
    }

    void deallocate(MemoryBlock blk)
    {
        if (!push(blk)) {
            doDeallocate(blk);
        }
    }

    bool owns(MemoryBlock _block)
    {
        return is_in(_block.length, min, max);
    }

    void deallocateAll()
    {
        while (size_ > 0) {
            doDeallocate(pop(max));
        }
    }

  private:
    struct Node
    {
        Node* next{};
    };
    A parent_;
    Node head_;
    std::size_t size_{};

    MemoryBlock doAllocate(std::size_t size)
    {
        MemoryBlock blk = parent_.allocate(max);
        if (blk)
            blk.length = size;
        return blk;
    }

    void doDeallocate(MemoryBlock blk)
    {
        if (blk)
        {
            blk.length = max;
            parent_.deallocate(blk);
        }
    }

    bool push(MemoryBlock blk)
    {
        bool ret = size_ < limit;
        if (ret) {
            auto new_node = static_cast<Node*>(blk.ptr);
            new_node->next = head_.next;
            head_.next = new_node;
            ++size_;
        }
        return ret;
    }

    MemoryBlock pop(std::size_t size)
    {
        auto node = head_.next;
        if (node) {
            head_.next = node->next;
            --size_;
        }
        return {node, node ? size : 0};
    }
    /* data */
}; /* class FreelistAllocator */
} /* namespace ya */
