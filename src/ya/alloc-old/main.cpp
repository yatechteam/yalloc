#include "Allocator.h"
#include "Allocators.h"
#include <iostream>
#include <vector>

using std::vector;
using std::pair;
using std::size_t;
using namespace ya;


template<class T, class Allocator>
class StdAllocator: private Allocator {
    public:
        using value_type = T;
};

int main(int argc, char *argv[]) {

    // Allocator a;
    //
    StackAllocator<1024> alloc;

    MemoryBlock block{};
    while ((block = alloc.allocate(16)) == true) {
        // std::cout << "ptr: " << block.ptr << std::endl;
        alloc.deallocate(block);
    }



    /*
    for (auto i = 1; i < 1024; ++i) {
        for (auto j = 0; j < 512; ++j) {
            vp.emplace_back(a.allocate(i), i);
        }

        for (auto p: vp) {
            a.deallocate(p.first, p.second);
        }
    }

    std::cout << "total: " << ya::DefaultAllocator::total_alloc  << std::endl;

    */
    return 0;
} /* end main() */
