#pragma once

#include "Block.h"

namespace ya {
namespace alloc {

template <class Primary, class Fallback>
struct FallbackAllocator : private Primary, private Fallback
{
    Block allocate(std::size_t size) noexcept
    {
        auto blk = Primary::allocate(size);
        if (!blk.ptr) {
            blk = Fallback::allocate(size);
        }
        return blk;
    }

    void deallocate(Block blk) noexcept
    {
        if (Primary::owns(blk)) {
            Primary::deallocate(blk);
        } else {
            Fallback::deallocate(blk);
        }
    }

    bool owns(Block blk) noexcept
    {
        return Primary::owns(blk) || Fallback::owns(blk);
    }
};
} /* namespace alloc */
} /* namespace ya */
