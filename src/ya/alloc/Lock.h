#pragma once

#include <atomic>
#include <mutex>

namespace ya {

struct dummy_lock
{
    void lock() noexcept {}
    void unlock() noexcept {}
};

class spin_lock
{
  public:
    spin_lock(bool init_lock = {})
        : lock_{init_lock}
    {
    }

    void lock() noexcept
    {
        bool expect{};
        while (!lock_.compare_exchange_weak(
                expect, true, std::memory_order_release, std::memory_order_relaxed))
            expect = false;
    }

    void unlock() noexcept
    {
        lock_.store(false);
    }

  private:
    std::atomic_bool lock_;
};

template <class L>
std::unique_lock<L> make_lock(L& l)
{
    return std::unique_lock<L>{l};
}

} /* namespace ya */
