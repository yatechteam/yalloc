#include "Block.h"
#include <iostream>
namespace ya {
namespace alloc {

std::ostream& operator<<(std::ostream& out, Block const& blk)
{
    return out << blk.ptr << ':' << blk.size;
}

void dummy(Block const&){}
} /* namespace alloc */
} /* namespace ya */
