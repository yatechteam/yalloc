#pragma once

#include "Block.h"
#include <cassert>

namespace ya {
namespace alloc {

struct NullAllocator
{
    Block allocate(std::size_t) noexcept
    {
        return Block{nullptr, 0};
    }
    void deallocate(Block blk) noexcept
    {
        assert(blk.ptr == nullptr);
        assert(blk.size == 0);
    }
    bool owns(Block blk) noexcept
    {
        return blk.ptr == nullptr;
    }
};

} /* namespace alloc */
} /* namespace ya */

