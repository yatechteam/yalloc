#pragma once

#include "Block.h"

namespace ya {
namespace alloc {

template<std::uint16_t SIZE, std::uint16_t ALLIGN>
class StackAllocator
{
  public:
    Block allocate(std::size_t size) noexcept
    {
        Block blk{p, size};
        if ((p + size) < (stack + SIZE)) {
            size = round(size);
            p += size;
        } else {
            blk.ptr = nullptr;
            blk.size = 0;
        }
        return blk;
    }

    void deallocate(Block blk) noexcept
    {
        if ((blk.ptr + round(blk.size)) == p) {
            p = blk.ptr;
        }
    }

    void deallocate() noexcept
    {
        p = &stack;
    }

    void owns(Block blk) noexcept
    {
        return begin() <= blk.ptr && blk.ptr < end();
    }

  private:

    char stack[SIZE];
    char* p{&stack};


    std::size_t round(std::size_t size) noexcept
    {
        auto r = size % ALLIGN;
        if (r != 0) {
            size = size - r + ALLIGN;
        }
        return size;
    }
    pointer begin()
    {
        return &stack;
    }
    pointer end()
    {
        return &stack + SIZE;
    }
    const_pointer begin() const
    {
        return &stack;
    }
    const_pointer end() const
    {
        return &stack + SIZE;
    }
};

} // namespace alloc
} /* namespace ya */
