#include <iostream>
#include <vector>
#include <cstring>

#include "FreeListAllocator.h"
#include "StatsAllocator.h"
#include "Mallocator.h"

using ya::alloc::FreeListAllocator;
using ya::alloc::Mallocator;
using ya::alloc::StatsAllocator;
using ya::alloc::Block;

template <class A>
void test(std::vector<Block> & v, A& fla, int i)
{
    for (int j = 0; j < i; j++) {
        Block b = fla.allocate((j % 256)+1);
        std::memset(b.ptr, 0, b.size);
        v.emplace_back(b);
    }
    for (auto b : v) {
        if (b)
            fla.deallocate(b);
    }
    v.resize(0);
}

int main(int argc, char* argv[])
{
#if 1
    FreeListAllocator<StatsAllocator<Mallocator>, 0, 256, 16, 20480> fla;
#else
    StatsAllocator<Mallocator> fla;
#endif
    int max = 1024;
    std::vector<Block> v;
    v.reserve(max);
    for (int k = 0; k < 10000; ++k)
        test(v, fla, max);
    return 0;
} /* end main() */
