#pragma once

#include "Block.h"
#include <iostream>
namespace ya {
namespace alloc {

struct Stats
{
    std::size_t alloc_count{};
    std::size_t dealloc_count{};
    std::size_t alloc_size{};
    std::size_t dealloc_size{};
};

template <class A>
class StatsAllocator
{
  public:
    ~StatsAllocator()
    {
        std::cout << "a:count: " << stats.alloc_count << std::endl;
        std::cout << "a:size : " << stats.alloc_size << std::endl;
        std::cout << "d:count: " << stats.dealloc_count << std::endl;
        std::cout << "d:size : " << stats.dealloc_size << std::endl;
    }
    Block allocate(std::size_t size)
    {
        auto blk = parent.allocate(size);
        if (blk) {
            stats.alloc_count++;
            stats.alloc_size += blk.size;
        }
        return blk;
    }
    void deallocate(Block blk)
    {
        if (blk) {
            stats.dealloc_count++;
            stats.dealloc_size += blk.size;
        }
        parent.deallocate(blk);
    }
    void deallocate()
    {
        parent.deallocate();
        stats.dealloc_size = stats.alloc_size;
        stats.dealloc_count = stats.alloc_count;
    }
    bool owns(Block blk) const
    {
        return parent.owns(blk);
    }

    Stats stats;

  private:
    A parent;
};

} /* namespace alloc */
} /* namespace ya */
