#pragma once

#include "Block.h"
#include <cstdlib>

namespace ya {
namespace alloc {

struct Mallocator
{
    Block allocate(std::size_t size) noexcept
    {
        return Block{malloc(size), size};
    }
    void deallocate(Block blk) noexcept
    {
        free(blk.ptr);
    }
    bool owns(Block blk) noexcept
    {
        return blk.ptr != nullptr;
    }
};

} /* namespace alloc */
} /* namespace ya */
