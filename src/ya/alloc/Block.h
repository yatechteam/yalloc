#pragma once

#include <cstdint>
#include <iosfwd>

namespace ya {
namespace alloc {

using pointer = void*;
using const_pointer = void const*;

struct Block
{
    pointer ptr;
    std::size_t size;

    constexpr operator bool() const
    {
        return ptr != nullptr;
    }
};

std::ostream& operator<<(std::ostream& out, Block const& blk);
void dummy(Block const&);

} /* namespace alloc */
} /* namespace ya */
