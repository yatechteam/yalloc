#pragma once

#include "Block.h"
#include "Lock.h"

namespace ya {
namespace alloc {

template <class A,
          std::size_t min_size,
          std::size_t max_size,
          std::size_t batch_size,
          std::size_t max_elements>
class FreeListAllocator
{
  public:
    ~FreeListAllocator()
    {
        deallocate();
    }
    Block allocate(std::size_t size)
    {
        if (in_range(size)) {
            Block blk = pop();
            if (!blk) {
                for (std::size_t i = 0; i < batch_size; ++i) {
                    push(blk);
                    blk = parent.allocate(max_size);
                    if (!blk)
                        break;
                }
            }
            if (blk) {
                return blk;
            }
        }
        return parent.allocate(size);
    }

    void deallocate(Block blk)
    {
        if (blk.size == max_size && count < max_elements) {
            push(blk);
        } else {
            parent.deallocate(blk);
        }
    }

    void deallocate()
    {
        Block blk;
        while ((blk = pop()) == true) {
            parent.deallocate(blk);
        }
    }

    bool owns(Block blk) const
    {
        return (blk.size == max_size || parent.owns(blk));
    }

  private:
    struct Node
    {
        Node* next{nullptr};
        Node() = default;
        Node(Node* n)
            : next{n}
        {
        }
    };

#ifdef SPIN_LOCK
    spin_lock lock;
#else
    dummy_lock lock;
#endif
    Node root{nullptr};
    std::size_t count{0};
    A parent;

    // functions
    constexpr static bool in_range(std::size_t size)
    {
        return min_size < size && size <= max_size;
    }

    void push(Block blk)
    {
        if (blk) {
            auto l = make_lock(lock);
            root.next = new (blk.ptr) Node{root.next};
            ++count;
        }
    }

    Block pop()
    {
        auto l = make_lock(lock);
        Node* next{root.next};
        if (next) {
            root.next = next->next;
            --count;
        }
        return Block{next, next ? max_size : 0};
    }
};

} /* namespace alloc */
} /* namespace ya */
