import os
def FlagsForFile( filename, **kwargs ):
    root = os.path.dirname(os.path.abspath(__file__))
    return {
            'flags': [ 
                '-x', 
                'c++',
                '-std=c++14', 
                '-Wall', 
                '-Wextra', 
                '-I'+ root + '/src', 
                '-I'+ root + '/yapu/src'
            ]
    }
